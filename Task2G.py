from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.analysis import polyfit
from floodsystem.plot import plot_water_level_with_fit
import datetime
import numpy as np

    
    

def evaluate_risk(station,gradient_weight,time_weight,height_weight):
#function to evaluate the risk of the stations
    dt=2
    p=4 #degree 4 against time
    risk_level = 0
    try:
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
   #Tries to see if there is data and can fit a polynomial to it
        data_consistent = True
        poly, d0 = polyfit(dates, levels, p)
        gradient = np.gradient(poly)
        risk_level += gradient_weight*gradient[-1]
        if gradient[:1] > 0:
                gradient2 =  list(np.gradient(gradient)) 
                gradient2.reverse()
                try: #tries to find lenght of time that the water has been rising
                        index_min = gradient.index(0)
                        risk_level += (len(gradient) - index_min)*time_weight
                except:
                        pass
        risk_level += station.relative_water_level()*height_weight
    except:
            data_consistent = False
    return risk_level,data_consistent




def print_risk_level(station_list):
#This function takes in the risk number and quantieses it'''
     risk_level = ""
     for station in station_list:
        risk_level = "SEVERE"
        if station[1] <1.6:
                risk_level = "HIGH"
        if station[1] < 1.2:
                risk_level = "MODERATE"
        if station[1] <0.7:
                risk_level = "LOW"

        print(station[0],risk_level)
     return station_list

def town_average(risk_list):
#'''This function takes the average station risk level and transforms it into a dictionary, with the town name as the key
#and with a tuple of the risk level and the number of stations within it'''
        town_dict = dict()
        for town_risk in risk_list:
                if town_risk[0] in town_dict:
                        tup_added = town_dict[town_risk[0]]
                        new_tup = (tup_added[0]+town_risk[1],(tup_added[1]+1))
                        town_dict[town_risk[0]] = new_tup

                else:
                        town_dict[town_risk[0]] = (town_risk[1],1)
        for town in town_dict:
                average_tup = town_dict[town]
                real_average = average_tup[0]/average_tup[1]
                town_dict[town] = real_average
        town_list = town_dict.items()
        town_list = list(town_list)
        return town_list



def run():
    risk_list = []
    stations = build_station_list()
    stations = stations[:100]               #Splice the stations, just for testing as all stations takes a very long time
    update_water_levels(stations)

#Attempted to remove incosistent data, didn't make much difference to results

    for station in stations:
            risk_level,data_good = evaluate_risk(station,1,0.002,5)
            risk_tuple = (station.town,risk_level)
            if data_good == True:
                risk_list.append(risk_tuple)
            else:
                risk_list.append((station.town,0)) #if no data is avlible, appaend a stupidly big number to show no data is there
    town_av = town_average(risk_list)
    town_av.sort(key=lambda x: x[1], reverse=True)

    print_risk_level(town_av)
    

if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    run()

